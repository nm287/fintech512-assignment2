package assignment2;
class Bottles {

	public String verse(int verseNumber) {
		if (verseNumber > 2) {
			return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n"
					+ "Take one down and pass it around, " + (verseNumber - 1) + " bottles of beer on the wall.\n";
		} else if (verseNumber == 2) {
			return "2 bottles of beer on the wall, " + "2 bottles of beer.\n"
					+ "Take one down and pass it around, " + "1 bottle of beer on the wall.\n";
		} else if (verseNumber == 1) {
			return "1 bottle of beer on the wall, " + "1 bottle of beer.\n" + "Take it down and pass it around, "
					+ "no more bottles of beer on the wall.\n";
		} else {
			return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
		}
	}

	public String verse(int startVerseNumber, int endVerseNumber) {
		if ( (startVerseNumber - endVerseNumber) == 1) {
			return verse(startVerseNumber) + "\n" + verse(endVerseNumber);
		}
		String Ver = "";
		if ( (startVerseNumber - endVerseNumber) > 1) {
			int diff = startVerseNumber - endVerseNumber;
			for (int i = diff; i > 0; i--) {
				Ver += verse(i) + "\n";
			}
			Ver += verse(0);
		}
		return Ver;
	}

	public String song() {
		String VerSong = "";
		for (int i = 99; i > 0; i--) {
			VerSong += verse(i) + "\n" ;
		}
		VerSong += verse(0);
		return VerSong;
	}
}
